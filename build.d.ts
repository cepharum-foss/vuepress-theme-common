export = CepharumVuePressThemesCommonBuild;

declare module CepharumVuePressThemesCommonBuild {
    function setup( options: BuildOptions, context: BuildContext ): object;

    type BuildOptions = object;

	/**
	 * Describes build context of VuePress.
	 *
	 * @see https://vuepress.vuejs.org/plugin/context-api.html#context-api
	 */
	interface BuildContext {
    	/**
		 * Indicates if site is built for production.
		 *
		 * @see https://vuepress.vuejs.org/plugin/context-api.html#ctx-isprod
		 */
    	isProd: boolean;

    	/**
		 * Lists current pages of sites.
		 *
		 * @see https://vuepress.vuejs.org/plugin/context-api.html#ctx-pages
		 */
    	pages: object[];

    	/** @see https://vuepress.vuejs.org/plugin/context-api.html#ctx-sourcedir */
    	sourceDir: string;

    	/** @see https://vuepress.vuejs.org/plugin/context-api.html#ctx-temppath */
    	tempPath: string;

    	/** @see https://vuepress.vuejs.org/plugin/context-api.html#ctx-outdir */
    	outDir: string;

    	/** @see https://vuepress.vuejs.org/plugin/context-api.html#ctx-base */
    	base: string;

    	/** @see https://vuepress.vuejs.org/plugin/context-api.html#ctx-writetemp */
    	writeTemp: string;
	}
}
