/**
 * Implements code for integrating common features with build process of a
 * consuming theme.
 *
 * @note This code is run as part of build process which is relying on CommonJS.
 *
 * @author Thomas Urban <thomas.urban@cepharum.de>
 * @file
 */

const Path = require( "path" );
const File = require( "fs" );

const Container = require( "markdown-it-container" );

/**
 * Maps functional HTML characters into related character entity literally
 * representing that character.
 *
 * @type {Object<string, string>}
 */
const HtmlCodes = {
	'"': "&quot;",
	"&": "&amp;",
	"<": "&lt;",
	">": "&gt;",
};

/**
 * Matches all functional HTML characters.
 *
 * @type {RegExp}
 */
const ptnHtmlUnsafeCharacters = new RegExp( "[" + Object.keys( HtmlCodes ).join( "" ) + "]", "g" );

/**
 * Sets up common features in context of a particular theme.
 *
 * This method is meant to be invoked as part of a theme's index.js entry file.
 *
 * @param {object} themeConfig exposes current site's theme configuration
 * @param {BuildContext} context describes build context
 * @returns {object} common features' contribution to the theme's resulting configuration
 */
function setup( themeConfig, context ) { // eslint-disable-line no-unused-vars
	const { componentsNamespace = "" } = themeConfig;

	return {
		extendMarkdown: md => {
			md.use( Container, "section", { render: ( tokens, idx ) => mdRenderContainer( "Section", tokens, idx ) } );
			md.use( Container, "tiles", { render: ( tokens, idx ) => mdRenderContainer( "Tiles", tokens, idx ) } );
			md.use( Container, "tile", { render: ( tokens, idx ) => mdRenderContainer( "Tile", tokens, idx ) } );
			md.use( Container, "accordion", { render: ( tokens, idx ) => mdRenderContainer( "Accordion", tokens, idx ) } );
			md.use( Container, "box", { render: ( tokens, idx ) => mdRenderContainer( "Box", tokens, idx ) } );

			// use <Reference> for rendering links instead of <a>
			// eslint-disable-next-line no-param-reassign,camelcase
			md.renderer.rules.link_open = function( tokens, idx ) {
				const token = tokens[idx];
				const attrs = [];

				const classes = token.attrGet( "class" );
				const href = token.attrGet( "href" );

				if ( classes ) {
					attrs.push( ` class="${escapeHtml( classes )}"` );
				}

				if ( href ) {
					attrs.push( ` href="${escapeHtml( href )}"` );
				}

				return `<Reference${attrs.join( "" )}>`;
			};

			// eslint-disable-next-line no-param-reassign,camelcase
			md.renderer.rules.link_close = function() {
				return `</Reference>`;
			};

			// use <ExtImage> for rendering images instead of <img>
			// eslint-disable-next-line no-param-reassign
			md.renderer.rules.image = function( tokens, idx ) {
				const token = tokens[idx];

				let src = token.attrGet( "src" );
				if ( !src ) {
					return "";
				}

				const extra = { type: undefined, width: undefined, height: undefined };
				const match = /#([^#]+)$/.exec( src );

				if ( match ) {
					const details = /^((?:[a-z_-][a-z0-9_-]*)(?:\/(?:[a-z_-][a-z0-9_-]*))*)?(?:@?([1-9]\d*)?x([1-9]\d*)?)?$/i.exec( match[1] );
					if ( !details ) {
						throw new Error( `invalid styling options in image URL ${src}` );
					}

					src = src.slice( 0, match.index );

					extra.type = details[1] == null ? undefined : `'${details[1]}'`;
					extra.width = parseInt( details[2] ) || undefined;
					extra.height = parseInt( details[3] ) || undefined;
				}

				const alt = escapeHtml( token.attrGet( "alt" ) || "" );
				const url = escapeHtml( src );

				return `<ExtImage url="${url}" alt="${alt}" :type="${extra.type}" :width="${extra.width}" :height="${extra.height}"></ExtImage>`;
			};
		},

		chainWebpack: config => {
			// associate prefix `@assets` with sub-folder named `assets` in site's project
			const siteAssetFolder = Path.resolve( context.sourceDir, "../assets/" );

			if ( File.existsSync( siteAssetFolder ) ) {
				config.resolve.alias.set( "@assets", siteAssetFolder );
			} else if ( !config.resolve.alias.has( "@assets" ) ) {
				config.resolve.alias.set( "@assets", Path.resolve( __dirname, "empty" ) );
			}

			// add support for linking local assets usually provided for download
			config.module.rule( "site-assets" )
				.test( /\.(pdf|zip|tar\.gz|tgz)(\?.*)?$/i )
				.use( "site-assets" )
				.loader( "file-loader" )
				.options( {
					name: "[path][name].[ext]?[sha512:contenthash:hex:32]",
				} );
		},

		globalUIComponents: [
			`${componentsNamespace}TermsPopup`,
		],
	};
}

/**
 * Encodes functional HTML characters using character entities.
 *
 * @param {string} raw string potentially containing functional HTML characters
 * @return {string} provided string with any functional HTML character disabled
 */
function escapeHtml( raw ) {
	return raw.replace( ptnHtmlUnsafeCharacters, code => HtmlCodes[code] );
}

/**
 * Compiles string describing HTML attribute for use in an opening HTML tag.
 *
 * @param {string} info string following sequence of colons opening container
 * @returns {string} HTML-encoded attributes extracted from provided string
 */
function mdRenderHtmlAttributes( info ) {
	const parameters = info.replace( /^\s*[a-z-]+\s*/i, "" );
	let args = "";

	if ( parameters.length > 0 ) {
		const ptn = /\s*(--)?([a-z]+(?:[-_]{1,2}[a-z0-9]+)*)([=:]([^\s"']\S*|(["']).*?\5))?/gi;
		const switches = {};
		let hasSwitches = false;
		let sub;

		while ( ( sub = ptn.exec( parameters ) ) != null ) {
			let key = sub[2].toLowerCase();

			if ( !sub[1] ) {
				key = key.replace( /-([a-z])/g, ( _, initial ) => initial.toUpperCase() );
			}

			if ( sub[3] ) {
				const value = sub[4].replace( /^(["'])(.*)\1$/, "$2" );

				args += ` ${key}="${escapeHtml( value )}"`;
			} else {
				hasSwitches = switches[key] = true;
			}
		}

		if ( hasSwitches ) {
			args += ` :switches="${escapeHtml( JSON.stringify( switches ) )}"`;
		}
	}

	return args;
}

/**
 * Renders markdown-it AST tokens of containers as HTML.
 *
 * @param {string} name name of token
 * @param {object[]} tokens whole list of tokens
 * @param {int} idx index of token to process into list of tokens
 * @return {string} HTML code representing selected token
 */
function mdRenderContainer( name, tokens, idx ) {
	const token = tokens[idx];

	if ( token.nesting === 1 ) {
		return `<${name}${mdRenderHtmlAttributes( token.info )}>\n`;
	}

	return `</${name}>\n`;
}


module.exports = {
	setup,
	escapeHtml,
	mdRenderContainer,
	mdRenderHtmlAttributes,
};
