import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon, FontAwesomeLayers, FontAwesomeLayersText } from "@fortawesome/vue-fontawesome";

/**
 * Sets up feature for supporting fontawesome icons.
 *
 * @param {{Vue: object}} context for enhancing VuePress-based application
 * @param {string|string[]|Object<string,any>|boolean} faConfig fontAwesome configuration, names of icons to inject
 * @returns {Promise} promises FontAwesome prepared
 */
export function setup( context, faConfig ) {
	const { Vue } = context;

	if ( Array.isArray( faConfig ) ) {
		faConfig.forEach( icon => {
			/*
			 * When requiring icons in site's config.js tree-shaking via import
			 * isn't available, so definition files per icon are required
			 * explicitly to help with tree shaking.
			 *
			 * However, that way, a different format is imported. library.add()
			 * expects information per icon as exported by index.js of either
			 * face, so compare
			 *
			 * require( "@fortawesome/pro-light-svg-icons" ).faSpinner
			 * require( "@fortawesome/pro-light-svg-icons/faSpinner" )
			 *
			 * to see the difference. Assume `icon` is containing what is
			 * provided in latter case, but library.add() is expecting whatever
			 * is provided in former case.
			 */
			if ( Array.isArray( icon.icon ) ) {
				library.add( icon );
			} else if ( icon.definition && Array.isArray( icon.definition.icon ) ) {
				library.add( icon.definition );
			}
		} );
	}

	Vue.component( "Icon", FontAwesomeIcon );
	Vue.component( "FontAwesomeIcon", FontAwesomeIcon );
	Vue.component( "FontAwesomeLayers", FontAwesomeLayers );
	Vue.component( "FontAwesomeLayersText", FontAwesomeLayersText );
}
