export = CepharumVuePressThemesCommonRuntime;

declare module CepharumVuePressThemesCommonRuntime {
    interface ThemeIntegration {
        Vue: object;
        options: object;
        router: object;
        siteData: object;
    }

    interface VuePressAmplePageDescriptor {
        path: string;
        regularPath: string;
        relativePath: string;
        title: string;
        frontmatter: { [key: string]: string };
    }

    interface VuePressSparsePageDescriptor {
        $sparse: true;
        path: string;
        regularPath: string;
    }

    type VuePressPageDescriptor = ( VuePressSparsePageDescriptor | VuePressAmplePageDescriptor );

    interface VueRoute {
		path: string;
		params: object;
		query: object;
		hash: string;
		fullPath: string;
		matched: object[];
		name: string;
		redirectedFrom: string;
	}

	interface VueRouteLocation {
    	path?: string;
    	name?: string;
    	replace?: boolean;
	}

    type VueRouteNextHandlerConfirm = () => void;
    type VueRouteNextHandlerError = ( error: Error ) => void;
    type VueRouteNextHandlerAbort = ( flag: false ) => void;
    type VueRouteNextHandlerRedirectSimple = ( url: string ) => void;
    type VueRouteNextHandlerRedirectComplex = ( target: VueRouteLocation ) => void;
    type VueRouteNextHandler = VueRouteNextHandlerConfirm | VueRouteNextHandlerError |
		VueRouteNextHandlerAbort | VueRouteNextHandlerRedirectSimple |
		VueRouteNextHandlerRedirectComplex;

    interface NavigationTreeSparseNode extends VuePressSparsePageDescriptor {
        children: Array<NavigationTreeNode>;
    }

    interface NavigationTreeAmpleNode extends VuePressAmplePageDescriptor {
        $parent?: NavigationTreeAmpleNode;
        $level: Array<NavigationTreeNode>;
        $depth: number;
        children: Array<NavigationTreeNode>;
    }

    type NavigationTreeNode = ( NavigationTreeSparseNode | NavigationTreeAmpleNode );

    interface NavigationTreeRange {
    	start: number;
    	stop: number;
    	optional?: boolean;
	}

    interface NavigationLink {
        label: string;
        url: string;
        active: boolean;
        selected: boolean;
        children: Array<NavigationLink>;
    }

    type NavigationTreeLevelSorter = ( a: NavigationTreeNode, b: NavigationTreeNode ) => number;

    function setup( context: ThemeIntegration ): void;
}
