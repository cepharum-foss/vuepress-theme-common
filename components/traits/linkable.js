import { ptnAssetPrefix } from "../../lib/utilities";

/**
 * @typedef {object} LinkableTrait
 * @type {{href: String}} props
 * @type {{isHtml(): boolean, linkUrl(): string, isLocal(): *, useNewTab(): *}} computed
 * @type {{linkUrl(): void}} watch
 * @type {{resolveLink(): void, click(*=): void}} methods
 */

/**
 * Implements trait for turning some component into linked component navigating
 * user to different view or some attachment when clicked.
 *
 * @example
 *
 * A minimum integration would look like this:
 *
 *     import { deepMerge } from "../../lib/deep-merge";
 *     import { Linkable } from "../traits/linkable";
 *
 *     const Foo = deepMerge( {}, Linkable, {
 *         name: "Foo",
 *
 *         data() {
 *             return {
 *                 resolvedHref: null,
 *                 resolving: false,
 *             };
 *         },
 *         mounted() {
 *             this.resolveLink();
 *         },
 *     } );
 *
 *     export default Foo;
 *
 * @type {LinkableTrait}
 */
export const Linkable = {
	props: {
		href: {
			type: String,
			default: "",
			note: "Optionally selects URL to open on clicking this tile.",
		},
	},
	watch: {
		linkUrl() {
			this.resolveLink();
		},
	},
	computed: {
		linkUrl() {
			const pureLink = String( this.href )
				.replace( /^!/, "" )
				.replace( ptnAssetPrefix, "" )
				.trim();

			const { redirect } = this.$site.themeConfig;
			if ( redirect.hasOwnProperty( pureLink ) ) {
				return redirect[pureLink];
			}

			return pureLink;
		},
		isLocal() {
			return !/^\w+:/.test( this.linkUrl );
		},
		isCustomApp() {
			return !this.isLocal && !/^(?:https?|ftps?|data):/i.test( this.linkUrl );
		},
		isHtml() {
			return /^#|(?:\.html?|\/(?:[^/.#?]*))(?:$|[#?])/i.test( this.linkUrl );
		},
		isLocalAnchor() {
			return /^#/i.test( this.linkUrl );
		},
		useNewTab() {
			return this.external || ( this.external == null && !this.isLocal && !this.isCustomApp ) || this.href.charAt( 0 ) === "!";
		},
	},
	methods: {
		click( event ) {
			if ( this.resolvedHref ) {
				this.$emit( "click", event );

				if ( !event.defaultPrevented ) {
					this.$globalEvents.$emit( "reference-clicked", event, this );

					if ( !event.defaultPrevented ) {
						if ( !this.useNewTab && this.isLocal && this.isHtml ) {
							if ( this.delay > 0 ) {
								setTimeout( () => {
									this.$router.push( this.resolvedHref );
								}, this.delay );
							} else {
								this.$router.push( this.resolvedHref );
							}

							event.preventDefault();
						} else if ( !this.isLocal ) {
							event.stopPropagation();
						}
					}
				}
			}
		},
		resolveLink( noActualResolution = false ) {
			if ( this.linkUrl && this.isLocal ) {
				if ( this.isHtml ) {
					if ( this.$isServer && !this.isLocalAnchor ) {
						const { pages } = this.$site;
						const numPages = pages.length;
						const ptnStripIndex = /(?:\/(?:index\.html)?)?$/;
						let found = false;

						let pageUrl = this.linkUrl.replace( /[#?].*$/, "" ).replace( ptnStripIndex, "" );
						if ( /^(?:\.\.?\/|[^./])/.test( pageUrl ) ) {
							pageUrl = require( "path" ).posix.resolve( this.$page.path.replace( /\/[^/]+$/, "" ), pageUrl );
						}

						for ( let i = 0; !found && i < numPages; i++ ) {
							if ( pages[i].path.replace( ptnStripIndex, "" ) === pageUrl ) {
								found = true;
							}
						}

						if ( !found ) {
							throw new ReferenceError( `dead link: reference on locally missing document ${this.linkUrl}` );
						}
					}
				} else {
					this.resolvedHref = undefined;
					this.resolving = true;

					if ( !noActualResolution ) {
						let loader;

						if ( this.theme ) {
							loader = import(
								// see https://webpack.js.org/api/module-methods/#dynamic-expressions-in-import
								`@theme/assets/${this.linkUrl}`
							);
						} else {
							loader = import(
								// see https://webpack.js.org/api/module-methods/#dynamic-expressions-in-import
								`@assets/${this.linkUrl}`
							);
						}

						loader
							.then( module => {
								this.resolvedHref = module.default;
							} )
							.catch( error => {
								if ( this.$isServer ) {
									throw new ReferenceError( `dead link: missing referenced asset file ${this.linkUrl}` );
								}

								console.error( `dead link: missing referenced asset file: ${error.message}` );
							} )
							.finally( () => {
								this.resolving = false;
							} );
					}

					return;
				}
			}

			this.resolvedHref = this.linkUrl;
			this.resolving = false;
		},
	},
};
