import { ptnAssetPrefix } from "../../lib/utilities";

/**
 * @typedef {object} ContainerTrait
 * @property {{containerStyle(): object, containerClassNames(): string[]}} computed exposes computed properties for use in template
 * @property {{image(): *}} watch declared function to invoke on changing image URL
 * @property {{containerLoadImage(): void}} methods declares method to be invoked on creating container
 * @property {{image: String, color: String, switches: Object, icon: (String|Array), text: String}} props declares accepted properties
 */

/**
 * Implements trait for common properties of several containers.
 *
 * @example
 *
 * A minimum integration would look like this:
 *
 *     import { deepMerge } from "../../lib/deep-merge";
 *     import { Container } from "../traits/container";
 *
 *     const Foo = deepMerge( {}, Container, {
 *         name: "Foo",
 *
 *         data() {
 *             return {
 *                 loadedImage: false,
 *             };
 *         },
 *         mounted() {
 *             this.containerLoadImage();
 *         },
 *     } );
 *
 *     export default Foo;
 *
 * @type {ContainerTrait}
 */
export const Container = {
	props: {
		color: {
			type: String,
			validator: value => value == null || /^#?[0-9a-z]{3}(?:[0-9a-z]{3}(?:[0-9a-z]{2})?)?$/i.test( value ),
			note: "Selects custom background color to apply on container. " +
			      "Color is provided in hex format with or without leading " +
			      "hash, e.g. `#66aa66`.",
		},
		text: {
			type: String,
			validator: value => value == null || /^#?[0-9a-z]{3}(?:[0-9a-z]{3}(?:[0-9a-z]{2})?)?$/i.test( value ),
			note: "Selects custom text color to apply on current container. " +
				  "Color is provided in hex format with or without leading " +
				  "hash, e.g. `#66aa66`.",
		},
		switches: {
			type: Object,
			default: () => ( {} ),
			note: "Maps names of boolean switches into truthy information " +
				  "whether either switch is enabled or not. Switches aren't " +
				  "controlling anything particular, but will be listed as " +
				  "classes of container to probably affect its styling via CSS.",
		},
		image: {
			type: String,
			default: "",
			note: "Selects image by its URL for showing in context of container.",
		},
		icon: {
			type: [ String, Array ],
			default: "",
			note: "Names icon to show on section."
		},
		faTransform: {
			type: String,
			default: "",
			note: "Selects one or more power transformations to apply on icon " +
			      "as supported by fortawesome."
		},
		faMask: {
			type: String,
			default: "",
			note: "Selects icon provided shape for masking actual icon of " +
			      "container."
		},
		faAnimate: {
			type: String,
			default: "",
			note: "Selects animation for icon as supported by fortawesome."
		},
	},
	watch: {
		image() {
			return this.containerLoadImage();
		},
	},
	computed: {
		containerClassNames() {
			const classes = Object.keys( this.switches || {} )
				.filter( name => !/^fa(?:-|Transform|Mask|Animate)/.test( name ) )
				.map( name => `type-${name}` );

			if ( this.image ) {
				classes.push( "with-image" );
				classes.push( this.loadedImage ? "image-loaded" : "image-loading" );
			} else {
				classes.push( "without-image" );
			}

			classes.push( this.icon ? "with-icon" : "without-icon" );
			classes.push( this.$slots.default ? "with-content" : "without-content" );

			if ( this.color || this.text ) {
				classes.push( "custom-color" );
			}

			return classes;
		},
		iconClassNames() {
			return Object.keys( this.switches || {} )
				.filter( name => /^fa-(?!fixed-width$)/.test( name ) )
				.map( name => name.replace( /^fa-/, "" ) );
		},
		iconFixedWidth() {
			return Boolean( this.switches.faFixedWidth || this.switches["fa-fixed-width"] );
		},
		containerStyle() {
			let assigned = false;
			const result = {};

			if ( this.color ) {
				assigned = true;
				result.backgroundColor = this.color;
			}

			if ( this.text ) {
				assigned = true;
				result.color = this.text;
			}

			return assigned ? result : undefined;
		},
	},
	methods: {
		containerLoadImage() {
			if ( this.image ) {
				const url = this.image.replace( ptnAssetPrefix, "" );

				setTimeout( () => {
					// see https://webpack.js.org/api/module-methods/#dynamic-expressions-in-import
					import( `@assets/${url}` )
						.then( image => {
							this.loadedImage = image.default;
						} )
						.catch( error => {
							console.error( `loading section image failed: ${error.message}` );
						} );
				}, 100 );
			}
		},
	},
};
