import Reference from "./basics/Reference";
import LinkList from "./navigation/LinkList";
import Navigator from "./navigation/Navigator";
import InjectedContent from "./InjectedContent";
import SiteLogo from "./SiteLogo";
import Drawer from "./Drawer";
import Tiles from "./container/Tiles";
import Tile from "./container/Tile";
import Section from "./container/Section";
import Box from "./container/Box";
import Accordion from "./container/Accordion";
import ExtImage from "./basics/ExtImage";
import Translations from "./navigation/Translations";
import TermsPopup from "./TermsPopup";
import Fa from "./Fa";

export default [
	Reference,
	LinkList,
	Navigator,
	InjectedContent,
	SiteLogo,
	Drawer,
	Tiles,
	Tile,
	Section,
	Box,
	Accordion,
	ExtImage,
	Translations,
	TermsPopup,
	Fa,
];
