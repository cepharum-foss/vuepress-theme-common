/**
 * Implements code for integrating common features with a consuming theme at
 * site's runtime.
 *
 * @note This code is run in browser relying on build Vue application relying on
 *       ES modules.
 *
 * @author Thomas Urban <thomas.urban@cepharum.de>
 * @file
 */


import Components from "./components";
import { getCurrentLocale, localize } from "./lib/i18n";
import { createTree, findCurrent } from "./lib/pages";
import { setup as fontAwesomeSetup } from "./features/fontawesome";

/**
 * Sets up integration of this common library with a particular theme's
 * implementation.
 *
 * @param {ThemeIntegration} context exposes information for app-level enhancements
 * @returns {void}
 */
export function setup( context ) {
	const { Vue, router, siteData } = context;
	const { pages, themeConfig } = siteData;

	if ( !themeConfig.features ) {
		themeConfig.features = {};
	}

	const { features, componentsNamespace = "" } = themeConfig;

	fixHeadTags();

	const siteTree = createTree( pages );

	Vue.prototype.$tree = siteTree;
	Vue.prototype.$globalEvents = new Vue();

	Components.forEach( component => {
		Vue.component( `${componentsNamespace}${component.name}`, component );
	} );


	let setupDone = Promise.resolve();

	const fontAwesome = features.fontAwesome = features.fontAwesome || features.fontawesome || features.fa ||
	                                           themeConfig.fontAwesome || themeConfig.fontawesome || themeConfig.fa ||
	                                           false;
	if ( fontAwesome ) {
		setupDone = setupDone.then( () => fontAwesomeSetup( context, fontAwesome ) );
	}


	router.beforeEach( handleRedirects );



	// patch router.push() to consume and ignore its exceptions e.g. thrown on intended redirections
	const originalPush = router.push;
	router.push = function( ...args ) {
		const result = originalPush.call( this, ...args );

		if ( result instanceof Promise ) {
			return result.catch( error => {
				if ( error ) {
					console.warn( `consumed router exception: ${error}` );
				} else {
					console.warn( `consumed unknown router exception probably due to some defined redirection thrown ${new Error().stack.replace( /^[\s\S]+?at/, "at" )}` );
				}
			} );
		}

		return result;
	};



	/**
	 * Handles redirections.
	 *
	 * @param {VueRoute} to describes route of view user is heading for
	 * @param {VueRoute} from describes route of view current transition was started at
	 * @param {VueRouteNextHandler} next must be invoked eventually
	 * @returns {void}
	 */
	function handleRedirects( to, from, next ) {
		setupDone
			.catch( error => {
				console.error( `setting up site failed: ${error.message}` );
			} )
			.then( () => {
				const numPages = pages.length;

				for ( let i = 0; i < numPages; i++ ) {
					const { path, frontmatter } = pages[i];

					if ( path === to.path ) {
						const redirect = localize( frontmatter.redirect, getCurrentLocale( findCurrent( siteTree, from.path || to.path ) ) );

						if ( redirect ) {
							if ( from.path === redirect ) {
								next( false );
							} else {
								next( redirect );
							}

							return;
						}

						break;
					}
				}

				const redirections = themeConfig.redirect || {};
				if ( redirections.hasOwnProperty( to.path ) ) {
					next( redirections[to.path] );
					return;
				}

				next();
			} )
			.catch( error => {
				console.error( `redirection failed: ${error}` );
			} );
	}

	/**
	 * Validates configured set of meta tags adding some required tag if missing.
	 *
	 * @returns {void}
	 */
	function fixHeadTags() {
		if ( Array.isArray( siteData.headTags ) ) {
			const declarations = siteData.headTags;
			const numDeclarations = declarations.length;

			for ( let i = 0; i < numDeclarations; i++ ) {
				const [ tag, { name } ] = declarations[i] || {};

				if ( tag.trim().toLowerCase() === "meta" && name === "format-detection" ) {
					return;
				}
			}

			declarations.push( [ "meta", { name: "format-detection", content: "telephone=no" } ] );
		} else {
			// eslint-disable-next-line no-param-reassign
			siteData.headTags = [
				[ "meta", { name: "format-detection", content: "telephone=no" } ],
			];
		}
	}
}
