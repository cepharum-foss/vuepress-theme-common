/* eslint-env browser */

/**
 * Fetches resource from remote service.
 *
 * @param {URL|string} url URL of resource to fetch
 * @param {string} method HTTP request method to use
 * @param {object} query query parameters to be appended to provided URL
 * @param {Blob|string|object} requestBody content of request body
 * @param {object} requestHeaders custom request headers
 * @returns {Promise<{status, headers, content}>} promises fetched resource's content combined with response's status code and headers
 */
export default function fetch( url, { method = "GET", query = null, requestBody = null, requestHeaders = null } = {} ) {
	if ( typeof window === "undefined" ) {
		throw new Error( "invalid attempt for fetching resource with client-only code" );
	}

	const names = query ? Object.keys( query ) : [];
	const qualifiedUrl = names.length > 0 ? `${url}?${names.map( n => `${encodeURIComponent( n )}=${encodeURIComponent( query[n] )}` ).join( "&" )}` : url;
	const qualifiedHeaders = Object.assign( {}, requestHeaders );
	const qualifiedBody = typeof requestBody === "object" && requestBody ? JSON.stringify( requestBody ) : requestBody;
	let desiredContentType;

	if ( qualifiedBody != null ) {
		if ( typeof requestBody === "object" && requestBody ) {
			desiredContentType = "application/json";
		} else {
			desiredContentType = typeof requestBody === "string" ? "text/plain" : "application/octet-stream";
		}
	}

	if ( window.fetch ) {
		const request = new Request( qualifiedUrl, {
			method: method || "GET",
			headers: qualifiedHeaders,
			body: qualifiedBody,
		} );

		if ( desiredContentType && !request.headers.has( "content-type" ) ) {
			request.headers.set( "content-type", desiredContentType );
		}

		return window.fetch( request )
			.then( response => {
				let promise;

				const mime = response.headers.get( "content-type" ).replace( /;.*$/, "" );
				switch ( mime ) {
					case "application/json" :
					case "text/json" :
						promise = response.json();
						break;

					case "text/plain" :
					case "text/css" :
					case "text/html" :
						promise = response.text();
						break;

					case "image/svg+xml" :
					case "text/xml" :
					case "application/xml" : {
						promise = response.text()
							.then( xml => new DOMParser().parseFromString( xml, mime ) );
						break;
					}

					default :
						promise = response.blob();
						break;
				}

				return promise.then( content => ( {
					status: response.status,
					headers: response.headers,
					content
				} ) );
			} );
	}


	return new Promise( ( resolve, reject ) => {
		const request = new XMLHttpRequest();

		request.addEventListener( "load", loaded );
		request.addEventListener( "error", reject );
		request.addEventListener( "abort", reject );

		request.open( method || "GET", qualifiedUrl );

		Object.keys( qualifiedHeaders ).forEach( name => {
			request.setRequestHeader( name, qualifiedHeaders[name] );
		} );

		request.send( qualifiedBody || null );

		// eslint-disable-next-line require-jsdoc
		function loaded() {
			const headers = new Map();

			request.getAllResponseHeaders()
				.split( /\r?\n/ )
				.forEach( line => {
					const match = /^\s*([^:]+)\s*:\s*(.*)\s*$/.exec( line );
					if ( match ) {
						headers.set( match[1].toLowerCase(), match[2] );
					}
				} );

			resolve( {
				status: request.status,
				headers,
				content: request.response,
			} );
		}
	} );
}
