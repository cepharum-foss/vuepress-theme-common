/* eslint-env browser */

/**
 * @typedef {object} TreeCacheEntry
 * @property {VuePressAmplePageDescriptor[]} pages list of pages tree was derived from
 * @property {NavigationTreeNode[]} root level of derived tree
 */

import { asBoolean } from "./utilities";


/**
 * Converts list of site's pages into multi-level hierarchy.
 *
 * @param {Array<VuePressPageDescriptor>} pages set of site's pages
 * @returns {Array<NavigationTreeNode>} root level of resulting hierarchy
 */
export function createTree( pages ) {
	if ( !Array.isArray( pages ) || !pages.length ) {
		return [];
	}

	// first pass: sort pages by path
	const sorted = pages.slice();

	sorted.sort( ( l, r ) => ( l.path ? r.path ? l.path.localeCompare( r.path ) : -1 : r.path ? 1 : 0 ) );

	// second pass: inject sparse node descriptors for deep hierarchies without index
	// e.g. if node /section/child/grandchild/sub/test.html is following /section/major.html
	let numItems = sorted.length;
	let previous = sorted[0].path.replace( /\/$/, "" );

	for ( let i = 1; i < numItems; i++ ) {
		const current = sorted[i].path.replace( /\/$/, "" );

		while ( previous && !current.startsWith( previous ) ) {
			previous = previous.replace( /\/[^/]*$/, "" );
		}

		if ( current.startsWith( previous ) ) {
			const sub = current.substr( previous.length );

			if ( sub[0] === "/" ) {
				const segments = sub.substr( 1 ).replace( /\/+$/, "" ).split( "/" );
				const numSegments = segments.length;

				for ( let j = 1; j < numSegments; j++ ) {
					sorted.splice( i, 0, {
						path: previous + "/" + segments.slice( 0, j ).join( "/" ) + "/",
						$sparse: true,
					} );

					numItems++;
					i++;
				}
			}
		}

		previous = current;
	}

	// third pass: convert linear list of node descriptors into tree
	const tree = [];

	if ( convertListToThread( sorted, null, tree, 0, numItems ) < numItems ) {
		throw new Error( "failed to put all pages into hierarchy" );
	}

	return tree;
}

/**
 * Sorts levels of tree according to selected property in either node's
 * front matter.
 *
 * @param {Array<NavigationTreeNode>} level list of nodes in tree (each probably referring to list of subordinated nodes)
 * @param {function(NavigationTreeNode, NavigationTreeNode):int} sorter callback invoked for comparing two nodes on sorting level
 * @param {boolean} deep controls whether sort subordinated levels as well (true)
 * @returns {Array<NavigationTreeNode>} provided list of nodes
 */
export function sortLevel( level, sorter, deep = true ) {
	level.sort( sorter );

	if ( deep ) {
		const numItems = level.length;

		for ( let i = 0; i < numItems; i++ ) {
			const { children } = level[i];

			if ( children.length > 0 ) {
				sortLevel( children, sorter );
			}
		}
	}

	return level;
}

/**
 * Converts slice of listed nodes into hierarchy of nodes.
 *
 * @note This method depends on properly sorted and qualified list of nodes.
 *
 * @param {Array<VuePressPageDescriptor>} items lists nodes to be converted
 * @param {NavigationTreeNode} parent reference on node to be parent of all converted nodes
 * @param {Array<NavigationTreeNode>} level collector for nodes of level in resulting hierarchy
 * @param {int} currentIndex index into list of nodes selecting node to convert next
 * @param {int} numItems total number of items in provided list
 * @returns {int} index of item succeeding last processed one
 */
export function convertListToThread( items, parent, level, currentIndex, numItems ) {
	const copy = Object.assign( { frontmatter: {} }, items[currentIndex] );

	Object.defineProperties( copy, {
		$parent: { value: parent, enumerable: false },
		$level: { value: level, enumerable: false },
		$depth: { value: parent ? parent.$depth + 1 : 0, enumerable: true },
		children: { value: [], enumerable: true },
	} );

	const copyMeta = copy.frontmatter;
	if ( !copyMeta.lang ) {
		copyMeta.lang = copyMeta.locale || ( parent && parent.frontmatter.lang ) || undefined;
	}

	level.push( copy );

	const prefix = copy.path.replace( /\/+$/, "" ) + "/";
	const prefixLength = prefix.length;

	for ( let index = currentIndex + 1; index < numItems; ) {
		const item = items[index];
		const path = item.path;

		if ( !path.startsWith( prefix ) ) {
			return index;
		}

		if ( /\/./.test( path.substr( prefixLength ) ) ) {
			throw new Error( `missing intermediate level on descending into tree (${prefix} -> ${path})` );
		}

		const moveTo = convertListToThread( items, copy, copy.children, index, numItems );
		if ( moveTo <= index ) {
			throw new Error( "iteration didn't advance as expected" );
		}

		index = moveTo;
	}

	return numItems;
}

/**
 * Deeply searches given level of tree for node matching selected URL.
 *
 * @param {Array<NavigationTreeNode>} level list of nodes in current level of hierarchy
 * @param {string} pageUrl URL of node to search
 * @returns {?NavigationTreeNode} found node or null if node is missing
 */
export function findCurrent( level, pageUrl ) {
	const numItems = level.length;

	for ( let i = 0; i < numItems; i++ ) {
		const item = level[i];

		if ( item.path === pageUrl && !item.$sparse ) {
			return item;
		}

		const found = findCurrent( item.children, pageUrl );
		if ( found ) {
			return found;
		}
	}

	return null;
}

/**
 * Find first ample (by means of non-sparse) node.
 *
 * The function descends into some existing hierarchy of nodes.
 *
 * @param {Array<NavigationTreeNode>} level level of nodes to search
 * @param {string} sortBy name of frontmatter property to look up per ample node for sorting
 * @returns {?NavigationTreeNode} found ample node, null when all nodes are sparse
 */
export function findAmpleNode( level, sortBy ) {
	const numItems = level.length;
	let minimum = Infinity;
	let ampleNode = null;

	for ( let i = 0; i < numItems; i++ ) {
		const node = level[i];
		const sortIndex = parseFloat( ( node.frontmatter || {} )[sortBy] );

		if ( isNaN( sortIndex ) ) {
			if ( !node.$sparse ) {
				ampleNode = node;
			}
		} else if ( sortIndex < minimum ) {
			minimum = sortIndex;
			ampleNode = node;
		}
	}

	if ( ampleNode ) {
		return ampleNode;
	}

	for ( let i = 0; i < numItems; i++ ) {
		const { children } = level[i];

		if ( children && children.length > 0 ) {
			const sub = findAmpleNode( children, sortBy );
			if ( sub ) {
				return sub;
			}
		}
	}

	return null;
}

/**
 * Collects roots of multiple threads of site hierarchy immediately or mediately
 * subordinated to provided node.
 *
 * @param {Array<NavigationTreeNode>} collector array populated with found roots
 * @param {NavigationTreeNode} current current node commonly superordinated to all resulting nodes
 * @param {NavigationTreeRange} range minimum and maximum level into site's hierarchy of root nodes to find
 * @param {NavigationTreeLevelSorter} sorter callback invoked for sorting siblings of single level
 * @returns {void}
 */
export function findSubordinatedRoots( collector, current, range, sorter ) {
	if ( current && current.$depth < range.start && current.children.length > 0 ) {
		const level = current.children.slice();

		sortLevel( level, sorter, false );

		if ( current.$depth + 1 === range.start ) {
			collector.splice( collector.length, 0, ...level );
		} else {
			const numItems = level.length;

			for ( let i = 0; i < numItems; i++ ) {
				findSubordinatedRoots( collector, level[i], range, sorter );
			}
		}
	}
}

/**
 * Sorts provided level of hierarchy and returns a list of link
 * descriptors.
 *
 * @param {NavigationTreeNode[]} level single level of nodes in hierarchy
 * @param {{start: int, stop: int}} range limits range of hierarchy levels to process
 * @param {string} currentPath URL path of current page
 * @param {string} sortBy name of frontmatter property used for sorting either level of hierarchy
 * @param {string} name name of navigation component resulting links will be used with (@see name of <Navigator>)
 * @param {int} slicedDepth indicates depth of level in resulting slice of tree
 * @returns {NavigationLink[]} related hierarchy of link descriptors
 */
export function convertLevel( level, range, currentPath, sortBy, name, slicedDepth = 0 ) {
	if ( !level || !level.length || level[0].$depth > range.stop ) {
		return [];
	}

	const numItems = level.length;
	const links = new Array( numItems );
	let write = 0;

	for ( let read = 0; read < numItems; read++ ) {
		let node = level[read];

		if ( node.$sparse ) {
			// sparse node adopts content of first ample descendant
			// or gets ignored if missing any ample descendant
			node = findAmpleNode( node.children, sortBy );
			if ( !node ) {
				continue;
			}
		}

		let hidden = false;

		const meta = node.frontmatter || {};
		const props = Object.keys( meta );
		const numProps = props.length;

		for ( let j = 0; j < numProps; j++ ) {
			const prop = props[j];

			switch ( prop.trim().toLowerCase() ) {
				case "demote" :
				case "hide" : {
					let hide = asBoolean( meta[prop], true );

					if ( typeof hide === "string" ) {
						const parts = hide.trim().split( /[\s;,]+/ );
						const numParts = parts.length;
						hide = false;

						for ( let k = 0; k < numParts; k++ ) {
							if ( parts[k] === name ) {
								hide = true;
								break;
							}
						}
					}

					if ( hide ) {
						hidden = true;
					}

					// process first matching frontmatter property, only
					j = Infinity;
				}
			}
		}

		if ( hidden ) {
			continue;
		}

		const label = meta.label || node.title;
		const url = node.path;
		const children = node.children;
		const $depth = node.$depth;

		const selected = url === currentPath || url === `${currentPath}/`;
		const active = selected || ( currentPath.startsWith( url ) && /^(\/|$)/.test( currentPath.substr( url.length ) ) > -1 );

		const sub = children.length ? convertLevel( children, range, currentPath, sortBy, slicedDepth + 1 ) : [];
		if ( sub.length || !node.$sparse ) {
			const link = {
				url, label, active, selected, $depth, depth: slicedDepth + 1,
			};

			link.children = active || !range.optional ? sub : [];

			links[write++] = link;
		}
	}

	links.splice( write );

	return links;
}
