/**
 * Deeply merges properties of source object into target object.
 *
 * @param {object} target target to be adjusted
 * @param {object} sources sources of properties to be written into target
 * @returns {object} provided target
 */
exports.deepMerge = function( target, ...sources ) {
	if ( !target ) {
		throw new TypeError( "missing target for deeply merging objects" );
	}

	const numSources = sources.length;

	for ( let i = 0; i < numSources; i++ ) {
		const source = sources[i];

		if ( source && typeof source === "object" && !Array.isArray( source ) ) {
			const names = Object.keys( source );
			const numNames = names.length;

			for ( let j = 0; j < numNames; j++ ) {
				const name = names[j];
				const value = source[name];

				switch ( name ) {
					case "__proto__" :
					case "constructor" :
						break;

					default :
						switch ( typeof value ) {
							case "object" :
								if ( value ) {
									// eslint-disable-next-line max-depth
									if ( Array.isArray( value ) ) {
										// eslint-disable-next-line no-param-reassign
										target[name] = Array.isArray( target[name] ) ? value.length ? target[name].concat( value ) : target[name] : value;
									} else {
										// eslint-disable-next-line no-param-reassign
										target[name] = exports.deepMerge( target[name] || {}, value );
									}
								}
								break;

							default :
								// eslint-disable-next-line no-param-reassign
								target[name] = value;
						}
				}
			}
		}
	}

	return target;
};
