/* eslint-env browser */

import { findCurrent } from "./pages";

export const labels = {
	de: "Deutsch",
	en: "English",
	fr: "Français",
};

export const ptnLocale = /^([a-z]{2,3})(?:-([a-z]{2}))?$/i;

/**
 * Fetches current locale to use.
 *
 * @param {?NavigationTreeNode} page selects page to consider on detecting current locale
 * @returns {string} current locale in BCP47 format, defaults to `en`
 */
export function getCurrentLocale( page = null ) {
	if ( page && page.frontmatter.lang ) {
		return page.frontmatter.lang;
	}

	if ( typeof localStorage !== "undefined" ) {
		const stored = localStorage.getItem( "locale" );
		if ( stored ) {
			return stored;
		}
	}

	if ( typeof window !== "undefined" ) {
		return window.navigator.language.toLowerCase();
	}

	return "en";
}

/**
 * Sets provided locale as current one.
 *
 * @param {string} locale locale to be current one
 * @returns {void}
 */
export function saveCurrentLocale( locale ) {
	if ( !ptnLocale.test( locale ) ) {
		throw new TypeError( `invalid or unsupported locale identifier: ${locale}` );
	}

	localStorage.setItem( "locale", locale.toLowerCase() );
}

/**
 * Returns the locale from provided list matching provided lookup.
 *
 * @param {string} lookup single locale identifier to look up in provided list
 * @param {string[]} locales list of locales to search for item matching lookup
 * @returns {string} matching item in provided list of locales, null if missing any match
 */
export function findMatchingLocale( lookup, locales ) {
	if ( lookup == null ) {
		return null;
	}

	if ( !ptnLocale.test( lookup ) ) {
		throw new TypeError( `invalid or unsupported locale identifier to look up: ${lookup}` );
	}

	if ( Array.isArray( locales ) ) {
		const numLocales = locales.length;
		let longestMatch = null;

		// eslint-disable-next-line no-param-reassign
		lookup = lookup.toLowerCase();

		for ( let i = 0; i < numLocales; i++ ) {
			const locale = locales[i];
			const lowerLocale = locale.toLowerCase();

			if ( ptnLocale.test( locale ) ) {
				if ( lookup === lowerLocale ) {
					return locale;
				}

				if ( lowerLocale.startsWith( lookup ) ) {
					// locale is more specific than lookup -> accept instantly
					return locale;
				}

				if ( lookup.startsWith( lowerLocale ) ) {
					// locale is less specific than lookup -> track as fallback
					if ( !longestMatch || locale.length > longestMatch ) {
						longestMatch = locale;
					}
				}
			}
		}

		return longestMatch;
	}

	return null;
}

/**
 * Looks up provided value assumed to be internationalized by having separate
 * values per locale for the value matching given or current locale.
 *
 * @note Internationalized values are objects mapping locale identifier into
 *       related value to use for that locale. Special property name `*` is
 *       supported as fallback.
 *
 * @param {any} value probably internationalized value
 * @param {string} locale locale to look up explicitly, omit for current locale
 * @returns {any} extracted value on providing internationalized input, provided input otherwise or null if missing value for locale
 */
export function localize( value, locale = null ) {
	if ( isInternationalized( value ) ) {
		if ( !locale ) {
			// eslint-disable-next-line no-param-reassign
			locale = getCurrentLocale();
		}

		const key = findMatchingLocale( locale, Object.keys( value ) );

		return key ? value[key] : value["*"] || null;
	}

	return value;
}

/**
 * Detects if provided value looks like internationalized value.
 *
 * @param {any} value value to be tested
 * @returns {boolean} true if value seems to be internationalized, false otherwise
 */
export function isInternationalized( value ) {
	if ( value && typeof value === "object" ) {
		const names = Object.keys( value );
		const numNames = names.length;

		for ( let i = 0; i < numNames; i++ ) {
			const name = names[i];

			if ( name !== "*" && !ptnLocale.test( name ) ) {
				return false;
			}
		}

		return true;
	}

	return false;
}

/**
 * Extracts names of properties in provided value addressing localized values.
 *
 * @param {any} value value to be processed, only objects cause non-empty result
 * @returns {string[]} lists property names suitable for addressing localized values
 */
export function getInternationalizingKeys( value ) {
	if ( !value || typeof value !== "object" ) {
		return [];
	}

	return extractLocales( Object.keys( value ), true );
}

/**
 * Removes items from array that don't contain valid locale identifier.
 *
 * @param {string[]|string} input list of items to filter
 * @param {boolean} acceptFallback set true to accept `*`, too
 * @returns {string[]} list of locale identifiers found in provided list
 */
export function extractLocales( input, acceptFallback = false ) {
	if ( !Array.isArray( input ) ) {
		return ptnLocale.test( input ) ? [String( input )] : [];
	}

	const numNames = input.length;
	const extracted = new Array( numNames );
	let write = 0;

	for ( let read = 0; read < numNames; read++ ) {
		const item = input[read];

		if ( ( acceptFallback && item === "*" ) || ptnLocale.test( item ) ) {
			extracted[write++] = item;
		}
	}

	extracted.splice( write );

	return extracted;
}

/**
 * Lists available translations of a page in context of provided tree.
 *
 * @param {object} siteConfig configuration of current site's theme
 * @param {Array<NavigationTreeNode>} tree current site's tree of pages
 * @param {string} path path of current page
 * @param {string[]} requiredLocales lists locales that must be included with the result
 * @param {string[]} sortingOrder lists locales (incl. non-required ones) in sorting order to be enforced
 * @return {Array<{label: string, language: string, locale: string, disabled: boolean, current: boolean, url: string}>} sorted list of available translations
 */
export function listTranslations( siteConfig, tree, path, requiredLocales = [], sortingOrder = [] ) {
	const customLabels = siteConfig.languages || {};

	// find all locales with immediate or mediate translations of current page
	const result = {};
	const node = findCurrent( tree, path );
	let iter = node;

	while ( iter ) {
		const meta = iter.frontmatter || {};
		const locales = extractLocales( Object.keys( meta ) );
		const numLocales = locales.length;

		for ( let i = 0; i < numLocales; i++ ) {
			const locale = locales[i];

			if ( !result.hasOwnProperty( locale ) ) {
				const language = locale.replace( /-.*$/, "" );

				result[locale] = {
					label: customLabels[locale] || labels[locale] || customLabels[language] || labels[language] || language,
					language,
					locale,
					url: meta[locale],
				};
			}
		}

		iter = iter.$parent;
	}

	// add entries for every missing but required locale
	const required = requiredLocales;
	const numRequired = required.length;

	for ( let i = 0; i < numRequired; i++ ) {
		const locale = required[i];

		if ( !result.hasOwnProperty( locale ) ) {
			const language = locale.replace( /-.*$/, "" );

			result[locale] = {
				label: customLabels[locale] || labels[locale] || customLabels[language] || labels[language] || language,
				language,
				locale,
				disabled: true,
			};
		}
	}

	// mark current locale's entry
	const keys = Object.keys( result );
	const currentLocale = findMatchingLocale( getCurrentLocale( node ), keys );

	if ( currentLocale && node ) {
		const current = result[currentLocale];

		current.current = true;
		current.disabled = false;
		current.url = node.path;
	}

	// apply desired sorting order
	const order = sortingOrder || required;
	const numResultKeys = keys.length;
	const numOrderKeys = order.length;
	const sorted = new Array( numResultKeys );
	let write = 0;

	for ( let i = 0; i < numOrderKeys; i++ ) {
		const key = order[i];
		const item = result[key];

		if ( item ) {
			sorted[write++] = item;
			result[key] = undefined;
		}
	}

	for ( let i = 0; i < numResultKeys; i++ ) {
		const item = result[keys[i]];

		if ( item ) {
			sorted[write++] = item;
		}
	}

	return sorted;
}
