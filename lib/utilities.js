/**
 * Matches prefix in URLs supported for addressing local assets.
 *
 * @type {RegExp}
 */
export const ptnAssetPrefix = /^@assets\//;

/**
 * Searches list of pages for the first one with selected tag.
 *
 * @param {VuePressPageDescriptor[]} pages list of site's pages
 * @param {string} tag tag name consisting of letters, digits, dashes and underscores, only
 * @returns {VuePressPageDescriptor} found page's descriptor
 */
export function findPageByTag( pages, tag ) {
	return findPagesByTag( pages, tag, 1 ).shift();
}

/**
 * Searches list of pages for all pages with selected tag.
 *
 * @param {VuePressPageDescriptor[]} pages list of site's pages
 * @param {string} tag tag name consisting of letters, digits, dashes and underscores, only
 * @param {number} limit maximum number of pages to return
 * @returns {VuePressPageDescriptor} found page's descriptor
 */
export function findPagesByTag( pages, tag, limit = Infinity ) {
	const matches = [];

	if ( Array.isArray( pages ) && /^[a-z0-9_-]+$/i.test( tag ) ) {
		const numPages = pages.length;
		const pattern = new RegExp( `(?:^|[\\s,;])${tag}(?:$|[\\s,;])`, "i" );

		for ( let i = 0; i < numPages; i++ ) {
			const page = pages[i];

			if ( pattern.test( page.frontmatter.tags || page.frontmatter.tag || "" ) ) {
				matches.push( page );

				if ( matches.length > limit ) {
					break;
				}
			}
		}
	}

	return matches;
}

/**
 * Maps provided value to boolean information supporting some human-readable
 * values.
 *
 * @param {any} value some value to normalize
 * @param {boolean} returnNonBoolean set true to return provided string unless it's actually boolean
 * @returns {boolean} normalized value
 */
export function asBoolean( value, returnNonBoolean = false ) {
	if ( /^\s*(?:y(?:es)?|true|on)\$*$/i.test( value ) ) {
		return true;
	}

	if ( /^\s*(?:no?|false|off|0)\$*$/i.test( value ) ) {
		return false;
	}

	if ( returnNonBoolean && value !== true && value !== false ) {
		return value;
	}

	return Boolean( value );
}
