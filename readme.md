# VuePress Theme Commons

_a set of common features for use with VuePress theme implementations_

## License

MIT

## About

When implementing themes for use with VuePress, this package is offering a common set of utilities and components. Most probably, you can use it in context of a particular site running its own theme, too.

## Setup

In your theme's package add the dependency:

```bash
yarn add @cepharum/vuepress-theme-common
```

Next, in your theme's **enhanceApp.js** file you need to set up this package:

```javascript
import * as Common from "@cepharum/vuepress-theme-common";

export default context => {
	Common.setup( context );
};
```

This will e.g. register commonly useful components and prepare use of FontAwesome icons.

### Troubleshooting

On developing a theme you might end up with issues in browser regarding **@cepharum/vuepress-theme-common** not found. This may be due to using symbolic links for linking your test site with the theme you are implementing. To fix it, you need to add the theme's node_modules folder to the set of folders webpack is looking for dependencies in site's configuration file **.vuepress/config.js**:

```javascript
const Path = require( "path" );
const File = require( "fs" );

module.exports = {
    ...,

    chainWebpack: config => {
        config.resolve.modules.prepend( "/path/to/your/theme/node_modules" );
    },
}
``` 


## Configuration Reference

### Features

tba.

### Components Namespace

tba.


## Components Reference

This package is providing a range components listed here.

### LinkList

The link list component is used to render a list or tree of links.

The required attribute **items** provides list of link items. Every item is an object consisting of the following properties:

  * **url** is providing the link's target URL.
  * **label** is the link's visible label.
  * **title** is an optional property providing the link's _title_, which is the text to show when hovering the link with the mouse for a while.
  * **selected** is an optional boolean marking whether the link is considered currently selected by means of representing the currently visible page.
  * **active** is another optional boolean marking whether the link is active by means of addressing a page superordinated to currently visible one.
  * **external** is an optional boolean marking whether linked target should be opened in a new tab or window of browser.
  * **children** is optionally providing a list of more links directly subordinated to current item. This is used in extended the list of links into a tree. 

### Navigator

This component is rendering a hierarchy of links addressing pages of current site.

```vue
<Navigator level="1"></Navigator>
```

#### Selecting Thread of Hierarchy

Its _mandatory_ attribute **level** is addressing a subset of current site's hierarchy to be presented. This hierarchy is adopted from the site's hierarchy of pages according to either page's URL. This is identical to the hierarchy of markdown files in file system unless using per-page permalinks.

The level selector may be internationalized, thus providing different values per current page's locale. 

The (optionally localized) level selector has the following format:

    [ anchor ] start [ [ modifier ] stop ]

* `anchor` may be name of a tag applied to a particular page. This page is used instead of current one for processing relative selections. Optionally use whitespace to separate `anchor` from `start` or stick with letters when picking tag names for pages.
* The integer value `start` is identifying absolute or relative depth of a level in site's hierarchy to start rendering links at. Relative depths refer to the current page or some selected anchor page.
* The integer value `stop` is optionally identifying absolute or relative depth of a level in site's hierarchy to stop rendering links at. Relative depths refer to the starting level. When omitted only start level is selected.
* A `modifier` may be given to adjust the way levels between `start` and `stop` are extracted. Current supported modifiers are:

  * `?` prevents deeper levels being rendered unless they are containing current page or directly subordinated to it.

With regards to `start` the following modes are supported:
  
* **Absolute:** A single positive integer value given without any sign selects all pages on related level of site's hierarchy limited to current page's branch.
  
  * Value `0` is addressing the home page, only. 
  * Value `1` is addressing all direct children to the home page
  * Value `2` is addressing all grand-children to the home page that are siblings of current page or siblings of any ancestor of current page.

  > All values exceeding current page's level result in an empty collection of links.

* **Relative:** A single integer value preceded by a sign is addressing a level of hierarchy related to current page's level and limited to current page's branch.
  
  * Value `-0` is ignored.
  * Value `-1` is addressing level of pages containing current one's parent.
  * Value `-2` is addressing level of pages containing current one's grant parent.
  * Value `+1` is addressing level of current page's children.
  * Value `+2` is addressing level of current page's grand children.
  
  Positive integers select subordinated levels, negative integers select superordinated ones. Selecting a level that does not exist in current hierarchy results in empty collection of link elements.

With regards to `stop` the following options are accepted:
  
* **Absolute Range:** When separating both integer values with a single dash, the second integer is selecting level of pages to include with resulting collection at most.
  
  * Value `1-2` is addressing all children and grandchildren of homepage.
  * Value `2-3` is addressing all children and grandchildren of current page's ancestor within set of homepage's children.
  * Value `2-2` is identical to `2`
  * Value `-1-2` is identical to `1-2` if current page is a grand child of homepage. If current page is subordinated to any such grand child, the value is identical to `-1`. If current page is a child of homepage, this value is identical to `0-2`.
    
  If second integer is addressing level superordinated to first one, no links are extracted, at all.
  
* **Relative Range:** When separating the values with a single plus, the second integer is selecting the number of subordinated levels to extract in addition to selected one.
  
  * Value `1+0` is identical to `1`.
  * Value `1+1` is identical to `1-2`.
  * Value `1+2` is identical to `1-3`.
  * Value `2+1` is identical to `2-3`.
  * Value `-1+1` is addressing pages in level subordinated to current page's level as well as either superordinated page's set of children.
  
* **Whole Threads:** When omitting second integer after providing dash separator, whole threads subordinated to either page of selected level are extracted.
  
  * Value `1-` is extracting the whole hierarchy of pages except for the homepage.
  * Value `2-` is extracting all descendants of current page's ancestor which is a child of homepage.
  * Value `-1-` is extracting all descendants of current page's parent and its siblings.

Last but not least, the `?` modifier may be insert between `start` and `stop` to limit rendered threads to cover context of current page, only. 

Some examples for **level** selector are:

* `2`
* `2-2`
* `home-de +2?-2`
* `refpage +2?+5`

#### Stacking Levels

By default, the resulting output is a nested set of unordered lists suitable for being rendered like a (partial) site map as well as for generating drop down menus or similar. In opposition to that boolean attribute **stacked** may be used to generate a sequence of non-nested unordered lists.

> You should combine this mode with `?` modifier in level selector.

#### Hiding Pages

The navigator control searches every linked page's frontmatter definition for case-insensitively named properties **hide** or **demote**. Search stops on first encounter of either property. Processing any found property's value as boolean switch, the page isn't linked in resulting navigator on truthy values.

> Hiding a page in navigation implicitly hides its subordinated pages. However, it is possible to start another (e.g. relative) navigation control at those subordinated pages.

An example page would look like this:

```yaml
---
hide: true
---

This page is available for retrieval. You can link it in other pages. But any 
covering navigation control won't be listing this page due to the single line 
in frontmatter above. 
```
